// Flattens Arrays
// All Arrays Found Within This Array Are "Unrolled"
// No Arrays Should Be Found Within Array Returned
const flatten = arr => {
    const newArr = []
    arr.forEach(ele => {
        if (Array.isArray(ele)) {
            // Recursively Calls Flatten
            flatten(ele).forEach(eleB => {
                newArr.push(eleB)
            })
        } else {
            newArr.push(ele)
        }
    })
    return newArr
}

// Test For Flatten Fn Above
// Returns False If Any Arrays Are Found Within Array
// This Denotes Flatten Doesn't Work
const testFlattenArray = arr => {
    arr.forEach(ele => {
        if (Array.isArray(ele)) {
            return false
        }
    })

    return true
}

const a = [[1, 2, [3]], 4]
const b = flatten(a)

console.log(b)
console.log(testFlattenArray(b))