#!/usr/bin/python

# Imports
import sys
import os
from datetime import datetime

# Global Variables
d = datetime.now()
timestamp = d.strftime("%m%d%Y") + "_" + d.strftime("%H%M%S")

rLog = sys.path[0] + "/WoWCombatLog.txt"
wLog = sys.path[0] + "/NewCombatLog_" + timestamp + ".txt"

# Intro
print("Auro's Combat Log Splitting Script!")

# Check Arguments
if len(sys.argv) == 1:
    print("\nPlease add at least starting Month/Day, and Hour")
    print("Example: ./CombatLogParse.py 05/28 16")
    print("This would split the combat log removing all events after 05/28 @ 4pm")
    print("Exiting Now!")
    sys.exit()

if len(sys.argv) < 3:
    print("\nPlease add at least starting Month/Day, and Hour")
    sys.exit()

# Setup Variables
date = sys.argv[1].split('/')
month = int(date[0])
day = int(date[1])
hour = int(sys.argv[2])

if (len(sys.argv) >= 4):
    rLog = sys.argv[3]

if (len(sys.argv) >= 5):
    wLog = sys.argv[4]

# Print Setup
print("Starting Date: " + str(month) + '/' + str(day))
print("Starting Hour: " + str(hour))
print("Source Log: " + rLog)
print("Destination Log: " + wLog)
confirm = raw_input("Would you like to continue? ")

if (confirm != 'y'):
    print("\nExiting Now!")
    sys.exit(0)

# Open File
try:
    f = open(rLog,'r')
    w = open(wLog, "w")
    print("\nFiles Opened");
except:
    print("\nLog Not Found, Or Not Writeable")
    print("\nExiting Now!")
    sys.exit(0)

out = f.readlines()

# Parse Log
print("\nParsing Log")

for line in out:
    split = line.split(' ')

    cLogDate = split[0].split('/')
    cLogMonth = int(cLogDate[0])
    cLogDay = int(cLogDate[1])

    timeSplit = split[1].split(':')
    cLogHour = int(timeSplit[0])

    if (cLogDay > day and cLogMonth == month) or cLogMonth > month:
        w.write(line)
    elif cLogDay == day and cLogMonth == month and cLogHour >= hour:
        w.write(line)

# Close File
print("\nLog Parsed @ " + wLog)
f.close()
w.close()
