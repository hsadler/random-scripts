#!/usr/bin/python3
import click
from PIL import Image
from os import fsencode, listdir, path, rename
import re
from sys import argv, exit
import uuid

# Functions
def buildAbsNewImageName(absSrcPath, height, width, uniqueId):
    directory = path.dirname(absSrcPath)
    extension = path.splitext(absSrcPath)[1]
    extension = str(extension).replace("'", "")
    newName = str.encode(''.join([str(width), '_', str(height), '_', str(uniqueId), extension]))
    return path.join(directory, newName)

def getImageDimensions(img):
    return (img.width, img.height)

def generateUniqueId():
    return uuid.uuid4()

def isImageAlreadyRenamed(src):
    pattern = re.compile(".*_{1}.*_{1}.*")
    # -edit is used by Topaz Gigapixel AI
    patternEdit = re.compile(".*_{1}.*_{1}.*-edit.*")
    basename = str(path.basename(src))
    return pattern.match(basename) and not patternEdit.match(basename)

def openImage(target):
    return Image.open(target)

def processRenameOfImage(src, target, alreadyRenamed, dryRun, force_rename):
    if (alreadyRenamed and not force_rename):
        print("Not Renaming ", src, "It's Already Been Renamed")
    elif (dryRun):
        print("[Dry Run] Renaming", src, "To", target)
    else:
        print("Renaming", src, "To", target)
        rename(src, target)

def renameImage(dry_run, force_rename, src):
    try:
        image = openImage(src)
        width, height = getImageDimensions(image)
        image.close();
        uniqueId = generateUniqueId()
        target = buildAbsNewImageName(src, height, width, uniqueId)
        alreadyRenamed = isImageAlreadyRenamed(src)
        processRenameOfImage(src, target, alreadyRenamed, dry_run, force_rename)
    except IOError as e:
        print("Error opening", src, "is it an image?")
        print(e)

# Main Script
@click.command()
@click.option('--dry-run', is_flag=True, help='When enabled the script will not actually rename but give you a preview')
@click.option('--force-rename', is_flag=True, help='When enabled even if a file is already matching the scripts file signature it will rename it anyways')
@click.argument('src')
def main(dry_run, force_rename, src):
    src = fsencode(src)
    isDir = path.isdir(src)
    isFile = path.isfile(src)

    if not isDir and not isFile:
        print("Src provided is not a directory or file, please validate input!")
        exit()

    if isDir:
        for file in listdir(src):
            renameImage(dry_run, force_rename, path.join(src, file))
    else:
        renameImage(dry_run, force_rename, src)

# Engage Click
if __name__ == '__main__':
    main()