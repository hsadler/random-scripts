#!/usr/bin/python

# Imports
import os
import sys
import zipfile
from datetime import datetime

# Global Variables
wowDir = "E:/Games/PC/World of Warcraft/_retail_"
wowClassicDir = "E:/Games/PC/World of Warcraft/_classic_"
wowClassicBetaDir = "E:/Games/PC/World of Warcraft/_classic_beta_"
addonBackupDir = "G:/Backups/WoWBackup"
addonClassicBackupDir = "G:/Backups/WoWClassicBackup"
addonClassicBetaBackupDir = "G:/Backups/WoWClassicBetaBackup"
folderChecked = True

# Functions
def folderCheck(check, path):
    if (check == True):
        check = os.path.exists(path)
    return check

def zipdir(base, path, zf):
    baseLen = len(base)
    for root, dirs, files in os.walk(path):
        for file in files:
            tempPath = os.path.join(root, file)
            zipPath = tempPath[baseLen:]
            zf.write(tempPath, zipPath)

# Intro
print("\nAuro's Addon Backup Script!!!\n")
print("1) Live")
print("2) Classic")
print("3) Beta")
print("*) Exit")
version = input()

if (version == "1"):
    main = wowDir
    backup = addonBackupDir
elif (version == "2"):
    main = wowClassicDir
    backup = addonClassicBackupDir
elif (version == "3"):
    main = wowClassicBetaDir
    backup = addonClassicBetaBackupDir
else:
    print("\nExiting!")
    sys.exit()

# Check Folders
folderChecked = folderCheck(folderChecked, main)
folderChecked = folderCheck(folderChecked, backup)
folderChecked = folderCheck(folderChecked, main + "/Interface")
folderChecked = folderCheck(folderChecked, main + "/Fonts")
folderChecked = folderCheck(folderChecked, main + "/WTF")

if (not folderChecked):
    print("\nFolders Not Detected! Exiting...")
    sys.exit()

# Build Timestamp
d = datetime.now()
timestamp = d.strftime("%m%d%Y") + "_" + d.strftime("%H%M%S")

# Build Zip
zf = zipfile.ZipFile(backup + '/' + timestamp + '.zip', mode='w')
print("\nZipping Up Files...")
zipdir(main, main + "/Interface", zf)
zipdir(main, main + "/Fonts", zf)
zipdir(main, main + "/WTF", zf)
print("Backup Finished @ " + backup + '/' + timestamp + '.zip')
