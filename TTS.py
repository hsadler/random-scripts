#!/usr/bin/python

import sys

if len(sys.argv) != 4:
    print('Not enough arguments');
    exit();

try:
    hours = int(sys.argv[1]);
    minutes = int(sys.argv[2]);
    seconds = int(sys.argv[3]);
except ValueError:
    print('Arguments are not integers');
    exit();

print((hours*60*60)+(minutes*60)+seconds);

exit();
